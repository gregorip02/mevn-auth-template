module.exports = {
  pluginOptions: {
    express: {
      shouldServeApp: true,
      serverDir: './server'
    }
  },
  devServer: {
    port: 8080
  }
}

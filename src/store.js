import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    user: JSON.parse(localStorage.user || '{}'),
    token: localStorage.token || ''
  },
  mutations: {
    authenticated (state, payload) {
      state.user = payload.user
      state.token = payload.token
    },

    unAuthenticated (state) {
      state.user = {}
      state.token = ''
    }
  }
})

store.subscribe((mutation, state) => {
  switch (mutation.type) {
    case 'authenticated':
      localStorage.token = state.token
      localStorage.user = JSON.stringify(state.user)
      Axios.defaults.headers.common['Authorization'] = state.token
      break

    case 'unAuthenticated':
      localStorage.removeItem('user')
      localStorage.removeItem('token')
      Axios.defaults.headers.common['Authorization'] = ''
      break
  }
})

export default store

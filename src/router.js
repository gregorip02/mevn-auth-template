import Vue from 'vue'
import VueRouter from 'vue-router'

import Productos from '@/views/productos/Main.vue'
import Clientes from '@/views/clientes/Main.vue'
import Facturas from '@/views/facturas/Main.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/facturas',
    name: 'facturas',
    component: Facturas
  }, {
    path: '/productos',
    name: 'productos',
    component: Productos
  }, {
    path: '/clientes',
    name: 'clientes',
    component: Clientes
  }]
})

export default router

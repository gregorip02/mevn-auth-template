import Vue from 'vue'
import Axios from 'axios'

// Components
import App from './App.vue'

// Complementos de vue
import router from './router'
import store from './store'
import './registerServiceWorker'
import './scss/app.scss'

const API_URL = process.env.VUE_APP_API_URL.replace('#APP_BASE#', window.location.hostname)

// Configuraciones de vue
Vue.config.silent = process.env.NODE_ENV === 'production'
Vue.config.productionTip = false

// Configuración del cliente http (axios)
Axios.defaults.baseURL = API_URL
Axios.defaults.headers.common['Authorization'] = store.state.token

// Una verificación sencilla de autenticación
const auth = () => {
  if (store.state.token) {
    Axios.get('/jwt/check').then(response => {
      // Auth ok, continue...
    }).catch(error => {
      if (error.hasOwnProperty('response')) {
        if (error.response.status === 401) {
          // Token invalid
          store.commit('unAuthenticated')
        }
      }
    })
  }
}

// Instancia de vue
new Vue({
  router,
  store,
  beforeCreate: () => {
    auth()
  },
  render: h => h(App)
}).$mount('#app')

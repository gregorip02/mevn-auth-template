# MEVN Template
Un template basico para empezar a construir una aplicación con
el stack MEVN. Incluye un sistema básico de autenticación y registro.
Ademas, usa bulma como framework css.

## Para empezar
```
$ npm install
$ cp .env.example .env
```
Configure sus credenciales para conectarse al servidor de mongodb en el
archivo .env

### Iniciando ambiente de desarrollo
```
$ npm run server:dev
$ npm run client:dev
```
Ejecute estos comandos en terminales separadas.

### Compile y comprima para producción
```
$ npm run client:production
$ npm run server:production
```

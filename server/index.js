const express = require('express')
const helmet = require('helmet')
const cors = require('cors')

const mongoose = require('./services/mongoose')
const router = require('./router')

module.exports = (app, http) => {
  /**
   * Servidor de base de datos iniciado
   */
  mongoose.connection.once('open', () => console.info('Mongodb ok'))

  /**
   * Implementación de cors.
   */
  app.use(cors({ credentials: true }))

  /**
   * Parser de peticiones.
   */
  app.use(express.json())
  app.use(express.urlencoded({ extended: true }))

  /**
   * Implementación de varias reglas de seguridad.
   */
  app.use(helmet())

  /**
   * Asignación de rutas a nivel API.
   */
  app.use('/api', router)
}

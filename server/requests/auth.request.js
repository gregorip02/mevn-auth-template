const Validator = require('../services/request-validator')

module.exports = (request, response, next) => {
  const validator = new Validator(request)

  validator.required(['identidad', 'password'])
  validator.min(['identidad', 'password'], 6)

  if (validator.hasErrors()) {
    return validator.errorsResponse(response)
  }

  return next()
}

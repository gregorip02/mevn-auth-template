const Validator = require('../services/request-validator')

module.exports = async (request, response, next) => {
  const validator = new Validator(request)

  validator.required(['nombre', 'identidad', 'password'])
  validator.min(['identidad', 'password'], 6)

  await validator.unique('users', { identidad: validator.attribute('identidad') }, 'identidad')

  if (validator.hasErrors()) {
    return validator.errorsResponse(response)
  }

  return next()
}

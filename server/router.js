const fs = require('fs')
const path = require('path')
const express = require('express')
const jwt = require('./services/jwt')

const UserMiddleware = require('./middlewares/user.middleware')

const router = express.Router()

/**
 * Un simple verificador de la cabecera authorization.
 * Este middleware a nivel de aplicación agrega el
 * atributo user, a el objeto request.
 */
router.use((request, response, next) => {
  if (request.headers.hasOwnProperty('authorization') && request.headers.authorization.length) {
    jwt.verify(request.headers.authorization, (error, decoded) => {
      if (error || !decoded) {
        delete request.user
        return response.status(401).json({
          passed: false,
          message: 'El token enviado no tiene valides'
        })
      }

      request.user = decoded.user
      return next()
    })
  } else {
    delete request.user
    return next()
  }
})

/**
 * Una simple ruta para verificar la valides de una autentificación.
 *
 * return next()
 */
router.get('/jwt/check', UserMiddleware, (request, response) => {
  return response.json({ passed: true, user: request.user })
})

/* Bootstrap all controllers here */
fs.readdirSync(path.resolve(__dirname, 'controllers')).forEach(file => {
  if (file.endsWith('.controller.js')) {
    let controller = require(path.resolve(__dirname, 'controllers', file))
    if (typeof controller === 'function') {
      // Por defecto, los controladores exponen una funcion
      // a la que express le inyecta la instancia [router]
      controller(router)
    } else if (controller.hasOwnProperty('routing')) {
      // En dado caso que el controlador no exporte por
      // defecto una funcion, verificara si existe la propiedad
      // routing: function(router). Que se encargara de publicar
      // las rutas.
      if (typeof controller.routing === 'function') {
        controller.routing(router)
      }
    } else {
      // [dev]: Imprimir un warning para aquellos controladores
      // que no exporten por default una functión.
      if (process.env.APP_ENV === 'development') {
        console.info('Es imposible inyectar la instancia [router] en este controlador: %s', file)
      }
    }
  }
})

module.exports = router

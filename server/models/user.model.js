const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')

/**
 * Definición del esquema.
 *
 * @type {mongoose}
 */
const Schema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true
  },
  identidad: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
}, { collection: 'users', timestamps: true })

/**
 * La forma en que mongoose expone los datos.
 *
 * @return {Object}
 */
Schema.methods.toJSON = function () {
  let user = this.toObject()
  delete user.password

  return user
}

/**
 * Ejecuta esta funcion antes de guardar un nuevo registro.
 *
 * @param  {Function} next
 *
 * @return next
 */
Schema.pre('save', function (next) {
  let user = this

  if (user.isModified('password')) {
    bcrypt.hash(user.password, 10, function (error, hash) {
      if (error) {
        return next(error)
      }
      // Guardar contraseña encryptada
      // en vez de en "plan text".
      user.password = hash
      return next()
    })
  } else {
    return next()
  }
})

module.exports = mongoose.model('User', Schema)

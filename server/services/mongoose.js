const mongoose = require('mongoose')

// Nombre de la base de datos
const name = process.env.MONGODB_NAME

// Credenciales de autenticación
const user = process.env.MONGODB_USER
const pass = process.env.MONGODB_PASS
const host = process.env.MONGODB_HOST
const port = process.env.MONGODB_PORT

mongoose.connect(`mongodb://${host}:${port}/${name}`, {
  user,
  pass,
  useNewUrlParser: true,
  useCreateIndex: true
})

mongoose.connection.on('error', (error) => {
  if (process.env.NODE_ENV === 'development') {
    console.error('Error de conexión con servidor de base de datos:', error)
  }

  process.exit(1)
})

module.exports = mongoose

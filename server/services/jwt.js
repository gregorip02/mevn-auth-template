const jwt = require('jsonwebtoken')

/**
 * Firma y crea un token con una marca de tiempo.
 *
 * @param Object user
 *
 * @return void
 */
exports.create = (user, callback) => {
  return jwt.sign({ user }, process.env.APP_SECRET, { expiresIn: '2h' }, callback)
}

/**
 * Verifica un token creado.
 *
 * @param String token
 *
 * @return void
 */
exports.verify = (token, callback) => {
  return jwt.verify(token, process.env.APP_SECRET, callback)
}

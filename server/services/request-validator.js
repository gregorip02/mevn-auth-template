const mongoose = require('./mongoose')

/**
 * Una simple clase para validar los datos enviados en las peticiones HTTP.
 *
 * @type {Class}
 * @author  @gregorip02 <gregori.pineres02@gmail.com>
 */
module.exports = class Validator {
  /**
   * Instancia de la clase
   *
   * @param  {Request} request express
   *
   * @return void
   */
  constructor (request) {
    this.request = request
    this.errors = []
  }

  /**
   * Retorna el atributo ha validar. En una piramide de importancia
   * los distribuye de la siguiente manera: request.body, request.params...
   *
   * @param  String attr
   *
   * @return String | false
   */
  attribute (attr) {
    if (this.request.body.hasOwnProperty(attr)) {
      return this.request.body[attr]
    } else if (this.request.params.hasOwnProperty(attr)) {
      return this.request.params[attr]
    }

    return false
  }

  /**
   * Verifica que exista el atributo en el cuerpo de la petición.
   *
   * @param  {Mixed} attr
   * @param  {String} message
   *
   * @return void
   */
  required (attrs, message = 'Este atributo es requerido') {
    if (typeof attrs === 'object') {
      for (let attr of attrs) {
        this.required(attr, message)
      }
    }
    if (typeof attrs === 'string') {
      let attr = this.attribute(attrs)
      if (!attr) {
        this.errors.push({ attr, message })
      }
    }
  }

  /**
   * Comprueba que un atributo no contenga mas de :size: caracteres.
   *
   * @param  {String} attrs
   * @param  {Number} size
   * @param  {String} message
   *
   * @return void
   */
  max (attrs, size, message = 'Este atributo no debe tener mas de :size: caracteres') {
    if (typeof attrs === 'object') {
      for (let attr of attrs) {
        this.max(attr, size, message)
      }
    }
    if (typeof attrs === 'string') {
      let attr = this.attribute(attrs)
      if (!attr || attr.length > size) {
        this.errors.push({ attr: attrs, message: message.replace(':size:', size) })
      }
    }
  }

  /**
   * Comprueba que un atributo no contenga menos de :min: caracteres.
   *
   * @param  {String} attrs
   * @param  {Number} size
   * @param  {String} message
   *
   * @return void
   */
  min (attrs, size, message = 'Este atributo no debe tener menos de :size: caracteres') {
    if (typeof attrs === 'object') {
      for (let attr of attrs) {
        this.min(attr, size, message)
      }
    }
    if (typeof attrs === 'string') {
      let attr = this.attribute(attrs)
      if (!attr || attr.length < size) {
        this.errors.push({ attr: attrs, message: message.replace(':size:', size) })
      }
    }
  }

  /**
   * Comprueba que un atributo no se encuentre registrado en bd.
   *
   * @param  {String}   collection
   * @param  {Object}   query
   * @param  {Function} callback
   *
   * @return callback
   */
  async unique (collection, query, attr, message = 'Parece que :attr: ya esta en uso') {
    let doc = await mongoose.connection.collection(collection).findOne(query)
    if (doc) {
      this.errors.push({
        attr,
        message: message.replace(':attr:', doc[attr])
      })
    }
  }

  /**
   * Comprueba si el array $errors contiene valores.
   *
   * @return {Boolean}
   */
  hasErrors () {
    return this.errors.length
  }

  /**
   * Retorna una respuesta de error al cliente.
   *
   * @param  {Response}  response express
   * @param  {Number}    code  422 http "Unprocessable Entity"
   *
   * @return {Response.json}
   */
  errorsResponse (response) {
    return response.status(422).json({
      passed: false,
      errors: this.getErrors(),
      message: 'La solicitud está bien formada pero fue imposible seguirla debido a errores semánticos.'
    })
  }

  /**
   * Retorna el array con los errores.
   *
   * @return {Arrat}
   */
  getErrors () {
    return this.errors
  }
}

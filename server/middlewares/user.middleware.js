/**
 * Un simple middleware que verifica que exista la propiedad "user" en la petición.
 *
 * @idea Este middleware debe validar que el usuario exista.
 *
 * @return next() | response()
 */
module.exports = (request, response, next) => {
  if (request.hasOwnProperty('user')) {
    return next()
  }

  return response.status(401).json({
    passed: false,
    message: 'No estas autenticado.'
  })
}

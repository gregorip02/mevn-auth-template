const bcrypt = require('bcryptjs')
const jwt = require('../services/jwt')
const User = require('../models/user.model')
const AuthRequest = require('../requests/auth.request')

module.exports = (router) => {
  router.post('/auth/login', AuthRequest, (request, response) => {
    // Obteniendo parametros de la petición.
    // Estos valores estan validados en $AuthRequest
    const { identidad, password } = request.body

    User.findOne({ identidad }).select('+password').exec((error, user) => {
      if (error || !user) {
        return response.json({
          passed: false,
          message: 'Tu nombre de usuario no esta registrado'
        })
      }

      bcrypt.compare(password, user.password, (error, passed) => {
        if (error || !passed) {
          return response.json({
            passed: false,
            message: user.nombre + ', esta no es tu contraseña'
          })
        }

        // Nuevo token
        jwt.create({ nombre: user.nombre, identidad: user.identidad }, (error, token) => {
          if (error || !token) {
            if (process.env.NODE_ENV === 'development') {
              // Error print for debuging
            }

            return response.status(500).json({
              passed: false,
              message: 'Error generando el token'
            })
          }

          request.user = user
          return response.json({ passed, token, user })
        })
      })
    })
  })
}

const jwt = require('../services/jwt')
const User = require('../models/user.model')
const RegisterRequest = require('../requests/register.request')

module.exports = router => {
  router.post('/auth/register', RegisterRequest, (request, response) => {
    // Obteniendo parametros de la petición.
    // Estos valores estan validados en $RegisterRequest
    const { nombre, identidad, password } = request.body

    // Creando un esquema de usuario
    const schema = new User({ nombre, identidad, password })

    schema.save((error, user) => {
      if (error || !user) {
        if (process.env.NODE_ENV === 'development') {
          // Error print for debuging
          console.error(error)
        }

        return response.status(500).json({
          passed: false,
          message: 'Error creando el usuario'
        })
      }

      // Creando un jwt para este usuario
      jwt.create({ nombre, identidad }, (error, token) => {
        if (error || !token) {
          if (process.env.NODE_ENV === 'development') {
            // Error print for debuging
          }

          return response.status(500).json({
            passed: false,
            message: 'Error generando el token'
          })
        }

        request.user = user
        return response.status(201).json({ passed: true, user, token })
      })
    })
  })
}
